package ru.pixabay.data.di

import android.content.Context
import androidx.room.Room
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import ru.pixabay.data.BuildConfig
import ru.pixabay.data.datasource.local.AppDB
import ru.pixabay.data.datasource.network.PixabayApi
import ru.pixabay.models.Constants
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataModule {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        val json = Json {
            ignoreUnknownKeys = true
            coerceInputValues = true
        }

        val okHttpBuilder = OkHttpClient().newBuilder()
        okHttpBuilder.addInterceptor {
            val request = it.request()
            val url = request.url.newBuilder()
                .addQueryParameter("key", BuildConfig.PIXABAY_API_KEY)
                .build()
            it.proceed(request.newBuilder().url(url).build())
        }

        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(json.asConverterFactory("application/json".toMediaType()))
            .client(okHttpBuilder.build())
            .build()
    }

    @Provides
    @Singleton
    fun providePixabayApi(retrofit: Retrofit): PixabayApi {
        return retrofit.create(PixabayApi::class.java)
    }

    @Provides
    @Singleton
    fun provideRoomDB(@ApplicationContext context: Context): AppDB {
        return Room.databaseBuilder(context, AppDB::class.java, Constants.DB_NAME)
            .addMigrations(MIGRATION1_2, MIGRATION2_3)
            .build()
    }
}


//миграции для примера, чтоб освоится в них, будут тут просто так, как напоминание, о том что миграции это говно
//словно выбираешь какой провод резать на бомбе
object MIGRATION1_2 : Migration(1, 2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("ALTER TABLE Image ADD COLUMN newField TEXT NOT NULL DEFAULT 'OH SHIT IM SORRY'")
    }
}

object MIGRATION2_3 : Migration(2, 3) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE ImageMigration (dbKey INTEGER NOT NULL, id INTEGER NOT NULL, pageURL TEXT NOT NULL, tags TEXT NOT NULL, largeImageURL TEXT NOT NULL, imageWidth INTEGER NOT NULL, imageHeight INTEGER NOT NULL, views INTEGER NOT NULL, downloads INTEGER NOT NULL, likes INTEGER NOT NULL, user_id INTEGER NOT NULL, user TEXT NOT NULL, userImageURL TEXT NOT NULL, PRIMARY KEY(dbKey))")
        database.execSQL("INSERT INTO ImageMigration (dbKey, id, pageURL, tags, largeImageURL, imageWidth, imageHeight, views, downloads, likes, user_id, user, userImageURL) SELECT dbKey, id, pageURL, tags, largeImageURL, imageWidth, imageHeight, views, downloads, likes, user_id, user, userImageURL FROM Image")
        database.execSQL("DROP TABLE Image")
        database.execSQL("ALTER TABLE ImageMigration RENAME TO Image")
    }

}