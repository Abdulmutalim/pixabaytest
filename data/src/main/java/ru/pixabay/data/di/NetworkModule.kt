package ru.pixabay.data.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.pixabay.data.datasource.network.FileApi
import ru.pixabay.data.datasource.network.ImagesApi
import ru.pixabay.data.datasource.network.PixabayApi
import ru.pixabay.data.datasource.network.VideosApi

@Module
@InstallIn(SingletonComponent::class)
abstract class NetworkModule {

    @Binds
    abstract fun bindImagesApi(i: PixabayApi): ImagesApi

    @Binds
    abstract fun bindVideosApi(i: PixabayApi): VideosApi

    @Binds
    abstract fun bindFileApi(i: PixabayApi): FileApi
}