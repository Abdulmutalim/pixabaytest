package ru.pixabay.data.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.pixabay.data.datasource.local.ImagesLayoutManagerTypePref
import ru.pixabay.data.datasource.local.PreferencesImpl
import ru.pixabay.data.datasource.local.SomePrefs1


@Module
@InstallIn(SingletonComponent::class)
abstract class SharedPreferenceModule {

    @Binds
    abstract fun bindImagesLayoutManagerTypePrefs(impl: PreferencesImpl): ImagesLayoutManagerTypePref

    @Binds
    abstract fun bindSomePref1(impl: PreferencesImpl): SomePrefs1

}