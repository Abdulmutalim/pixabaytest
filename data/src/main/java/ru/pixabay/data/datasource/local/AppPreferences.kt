package ru.pixabay.data.datasource.local

import android.content.Context
import androidx.annotation.StringRes
import dagger.hilt.android.qualifiers.ApplicationContext
import ru.pixabay.data.R
import ru.pixabay.models.Constants
import ru.pixabay.models.enums.LayoutManagerType
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferencesImpl @Inject constructor(@ApplicationContext private val context: Context)
    : ImagesLayoutManagerTypePref, SomePrefs1 {
    private val prefs = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE)
    private val editor = prefs.edit()

    private fun getKey(@StringRes res: Int): String {
        return context.getString(res)
    }

    override fun saveImagesLayoutManagerType(type: String) {
        editor.putString(getKey(R.string.imagesLayoutManagerKey), type).apply()
    }

    override fun getImagesLayoutManagerType(): String {
        return prefs.getString(getKey(R.string.imagesLayoutManagerKey), LayoutManagerType.Linear.name) ?: ""
    }
}

interface ImagesLayoutManagerTypePref {
    fun saveImagesLayoutManagerType(type: String)
    fun getImagesLayoutManagerType(): String
}

interface SomePrefs1 {
}