package ru.pixabay.data.datasource.network

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap
import ru.pixabay.models.Images


interface ImagesApi {
    @GET(".?safesearch=true")
    suspend fun loadImages(@Query("page") page: Int): Response<Images>

    @GET(".")
    suspend fun searchImages(
        @Query("q") query: String,
        @Query("page") page: Int,
        @Query("colors") colors: List<String>?,
        @QueryMap map: Map<String, String>,
    ): Response<Images>
}