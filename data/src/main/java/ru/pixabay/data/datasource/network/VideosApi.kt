package ru.pixabay.data.datasource.network

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap
import ru.pixabay.models.Videos

interface VideosApi {
    @GET("videos/")
    suspend fun searchVideos(
        @Query("q") query: String,
        @Query("page") page: Int,
        @QueryMap map: Map<String, String>
    ): Response<Videos>
}