package ru.pixabay.data.datasource.network

import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Streaming
import retrofit2.http.Url

interface FileApi {
    @Streaming
    @GET
    suspend fun downloadFile(@Url url: String): ResponseBody
}