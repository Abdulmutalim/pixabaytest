package ru.pixabay.data.datasource.local

import androidx.room.*
import ru.pixabay.models.Images

@Database(
    entities = [Images.Image::class],
    version = 3)
abstract class AppDB : RoomDatabase() {
    abstract fun imagesDao(): ImagesDao
}

@Dao
interface ImagesDao {

    @Query("SELECT COUNT(*) FROM Image")
    suspend fun imageRowsCount(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertImages(images: List<Images.Image>)

    @Query("SELECT * FROM Image LIMIT 20 OFFSET :page*20")
    suspend fun getImages(page: Int): List<Images.Image>

    @Query("DELETE FROM Image")
    suspend fun clearImages()
}