package ru.pixabay.data.repository

import retrofit2.Response
import ru.pixabay.data.datasource.network.PixabayApi
import ru.pixabay.domain.repository.VideosRepository
import ru.pixabay.models.Videos
import javax.inject.Inject

class VideosRepositoryImpl @Inject constructor(
    private val api: PixabayApi
) : VideosRepository {
    override suspend fun searchVideos(
        query: String,
        page: Int,
        queryMap: Map<String, String>
    ): Response<Videos> {
        return api.searchVideos(query, page, queryMap)
    }
}