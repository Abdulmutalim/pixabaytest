package ru.pixabay.data.repository

import ru.pixabay.data.datasource.local.AppDB
import ru.pixabay.data.datasource.local.ImagesLayoutManagerTypePref
import ru.pixabay.data.datasource.network.ImagesApi
import ru.pixabay.data.datasource.network.PixabayApi
import ru.pixabay.domain.repository.ImagesRepository
import ru.pixabay.models.Images
import javax.inject.Inject

class ImagesRepositoryImpl @Inject constructor(
    private val api: ImagesApi,
    private val db: AppDB,
    private val pref: ImagesLayoutManagerTypePref
) :
    ImagesRepository {
    override suspend fun getImages(page: Int) = api.loadImages(page)
    override suspend fun searchImages(
        query: String,
        page: Int,
        colors: List<String>?,
        map: Map<String, String>
    ) = api.searchImages(query, page, colors, map)

    override suspend fun insertDBImages(images: List<Images.Image>) =
        db.imagesDao().insertImages(images)

    override suspend fun imagesDbRowsCount() = db.imagesDao().imageRowsCount()
    override suspend fun getDbImages(page: Int) = db.imagesDao().getImages(page)
    override suspend fun clearDbImages() = db.imagesDao().clearImages()

    override fun saveImagesLayoutManagerType(type: String) {
        pref.saveImagesLayoutManagerType(type)
    }

    override fun getImagesLayoutManagerType(): String {
        return pref.getImagesLayoutManagerType()
    }
}