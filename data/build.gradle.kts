plugins {
    id(BuildPlugins.androidLibrary)
    id(BuildPlugins.kotlinAndroid)
    id(BuildPlugins.kotlinKapt)
}

android {
    compileSdk = 31

    defaultConfig {
        minSdk = 23
        targetSdk = 31

        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
            buildConfigField("String", "PIXABAY_API_KEY", "\"23669754-11c86f7c5c0d5f99bc6978371\"")
            buildConfigField("String", "BASE_URL", "\"https://pixabay.com/api/\"")
        }

        debug {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
            buildConfigField("String", "PIXABAY_API_KEY", "\"23669754-11c86f7c5c0d5f99bc6978371\"")
            buildConfigField("String", "BASE_URL", "\"https://pixabay.com/api/\"")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_11.toString()
    }
}

dependencies {
    implementation(project(":models"))
    implementation(project(":domain"))

    //network
    implementation(Dependencies.retrofit)
    implementation(Dependencies.loggingInterceptor)
    implementation(Dependencies.okhttp3)
    implementation(Dependencies.serializationConverter)
    implementation(Dependencies.kotlinxSerialization)
    //room
    implementation(Dependencies.room)
    implementation(Dependencies.roomKtx)
    kapt(Dependencies.roomCompiler)

    //dagger
    implementation(Dependencies.hilt)
    kapt(Dependencies.hiltCompiler)
}