package ru.pixabay.domain.repository

import retrofit2.Response
import ru.pixabay.models.Images

interface ImagesRepository {
    suspend fun getImages(page: Int): Response<Images>
    suspend fun searchImages(
        query: String,
        page: Int,
        colors: List<String>?,
        map: Map<String, String>
    ): Response<Images>

    suspend fun imagesDbRowsCount(): Int
    suspend fun getDbImages(page: Int): List<Images.Image>
    suspend fun clearDbImages()
    suspend fun insertDBImages(images: List<Images.Image>)

    fun saveImagesLayoutManagerType(type: String)
    fun getImagesLayoutManagerType(): String
}