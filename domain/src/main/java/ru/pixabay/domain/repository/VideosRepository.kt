package ru.pixabay.domain.repository

import retrofit2.Response
import ru.pixabay.models.Videos

interface VideosRepository {
    suspend fun searchVideos(query: String, page: Int, queryMap: Map<String, String>): Response<Videos>
}