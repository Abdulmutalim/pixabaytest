package ru.pixabay.domain.usecase

import ru.pixabay.domain.repository.ImagesRepository
import ru.pixabay.models.enums.LayoutManagerType
import javax.inject.Inject

class SaveImgLayoutManagerTypeUseCase @Inject constructor(private val repo: ImagesRepository) {
    fun save(type: LayoutManagerType) {
        repo.saveImagesLayoutManagerType(type.name)
    }
}