package ru.pixabay.domain.usecase

import ru.pixabay.domain.repository.ImagesRepository
import javax.inject.Inject

class ClearDBImagesUseCase @Inject constructor(private val repo: ImagesRepository) {
    suspend fun clearImages() = repo.clearDbImages()
}