package ru.pixabay.domain.usecase

import ru.pixabay.domain.repository.ImagesRepository
import javax.inject.Inject

class SearchImagesUseCase @Inject constructor(private val repo: ImagesRepository) {
    suspend fun searchImages(
        query: String,
        page: Int,
        colors: List<String>?,
        map: Map<String, String>
    ) = repo.searchImages(query, page, colors, map)
}