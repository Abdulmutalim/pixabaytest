package ru.pixabay.domain.usecase

import ru.pixabay.domain.repository.VideosRepository
import javax.inject.Inject

class SearchVideosUseCase @Inject constructor(private val repo: VideosRepository) {
    suspend fun search(query: String, page: Int, queryMap: Map<String, String>) =
        repo.searchVideos(query, page, queryMap)
}