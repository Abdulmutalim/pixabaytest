package ru.pixabay.domain.usecase

import ru.pixabay.domain.repository.ImagesRepository
import ru.pixabay.models.Images
import javax.inject.Inject

class InsertDBImagesUseCase @Inject constructor(private val repo: ImagesRepository) {
    suspend fun insertImages(images: List<Images.Image>) = repo.insertDBImages(images)
}