package ru.pixabay.domain.usecase

import ru.pixabay.domain.repository.ImagesRepository
import javax.inject.Inject

class GetDBImagesCountUseCase @Inject constructor(private val repo: ImagesRepository) {
    suspend fun getRowsCount() = repo.imagesDbRowsCount()
}