package ru.pixabay.domain.usecase

import ru.pixabay.domain.repository.ImagesRepository
import javax.inject.Inject

class GetDBImagesUseCase @Inject constructor(private val repo: ImagesRepository) {
    suspend fun loadImages(page: Int) = repo.getDbImages(page)
}