package ru.pixabay.domain.usecase

import ru.pixabay.domain.repository.ImagesRepository
import ru.pixabay.models.enums.LayoutManagerType
import javax.inject.Inject

class GetImgLayoutManagerTypeUseCase @Inject constructor(private val repo: ImagesRepository) {
    fun get(): LayoutManagerType {
        return when(repo.getImagesLayoutManagerType()) {
            LayoutManagerType.Linear.name -> LayoutManagerType.Linear
            LayoutManagerType.RicardoFlex.name -> LayoutManagerType.RicardoFlex
            LayoutManagerType.Grid.name -> LayoutManagerType.Grid
            LayoutManagerType.Staggered.name -> LayoutManagerType.Staggered
            else -> LayoutManagerType.Linear
        }
    }
}