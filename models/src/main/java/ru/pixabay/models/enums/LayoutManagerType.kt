package ru.pixabay.models.enums

sealed class LayoutManagerType(val name: String) {
    object Linear : LayoutManagerType("linear")
    //FlexboxLayoutManager параша не раотает как надо, при возвращении на экран списка
    //это говно пересоздает вьюходеры прямо до видимой позиции,
    //тоесть если ты вернулся и позиция списка 1000, он создаст 1000 вьюхлодеров
    //поэтому начинаеа виснуть, тем сильнее, чем дальше скролл
    //изза этого использую ChipsLayoutManger, и то с ограничением по высоте для итемов
    object RicardoFlex : LayoutManagerType("RicardoMilosMegaFlex")
    object Grid: LayoutManagerType("grid")
    object Staggered: LayoutManagerType("staggered")
}