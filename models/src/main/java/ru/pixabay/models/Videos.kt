package ru.pixabay.models

import android.os.Parcelable
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
data class Videos(val total: Int, val totalHits: Int, val hits: List<Video>) {

    @Serializable
    @Parcelize
    data class Video(
        val id: Long,
        val pageURL: String,
        val tags: String,
        val duration: Int,
        val picture_id: String,
        val views: Int,
        val downloads: Int,
        val likes: Int,
        val comments: Int,
        val user_id: Long,
        val user: String,
        val userImageURL: String,
        val videos: VideoVariants
    ) : Parcelable {

        @Transient
        @IgnoredOnParcel
        var lastPlayerSeek = 0L

        @Serializable
        @Parcelize
        data class VideoVariants(
            val large: VideoData,
            val medium: VideoData,
            val small: VideoData,
            val tiny: VideoData
        ) : Parcelable

        @Serializable
        @Parcelize
        data class VideoData(
            val url: String,
            val width: Int,
            val height: Int,
            val size: Int
        ) : Parcelable
    }
}
