package ru.pixabay.models.eventbusmodels

class ApplyImagesFiltersEvent(val map: Map<String, String>, val colors: List<String>)