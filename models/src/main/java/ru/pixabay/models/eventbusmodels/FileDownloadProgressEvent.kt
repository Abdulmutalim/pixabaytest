package ru.pixabay.models.eventbusmodels

class FileDownloadProgressEvent(val fileId: Long, val progress: Int)
class FileDownloadStarted(val fileId: Long)
class FileDownloadEnded(val fileId: Long)
class FileDownloadError()