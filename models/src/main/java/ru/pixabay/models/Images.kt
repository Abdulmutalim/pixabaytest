package ru.pixabay.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
data class Images(
    val total: Int,
    val totalHits: Int,
    val hits: List<Image>
) {

    @Parcelize
    @Serializable
    @Entity
    data class Image(
        val id: Long,
        val pageURL: String,
        val tags: String,
        val largeImageURL: String,
        val imageWidth: Int,
        val imageHeight: Int,
        val views: Int,
        val downloads: Int,
        val likes: Int,
        val user_id: Int,
        val user: String,
        val userImageURL: String
    ) : Parcelable {
        @PrimaryKey(autoGenerate = true)
        @Transient
        @IgnoredOnParcel
        var dbKey: Long = 0

        fun getUserNameFirstChar(): String{
            return user.first().toString()
        }

        fun getCapitalizedUserName(): String {
            val firstChar = getUserNameFirstChar()
            return user.replaceFirst(firstChar, firstChar.uppercase(), false)
        }
    }
}
