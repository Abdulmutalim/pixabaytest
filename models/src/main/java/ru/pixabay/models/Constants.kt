package ru.pixabay.models

object Constants {
    const val PIXABAY_SITE_URL = "https://pixabay.com/"
    const val DB_NAME = "pixabay_db"
    const val PREFS_NAME = "pixabay_prefs"
}