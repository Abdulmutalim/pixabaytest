buildscript {
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath (ClasspathDependecies.androidGradlePlugin)
        classpath (ClasspathDependecies.koltinGradlePlugin)
        classpath (ClasspathDependecies.daggerHiltPlugin)
        classpath (ClasspathDependecies.kotlinSerialization)
        classpath (ClasspathDependecies.navComponentSafeArgs)
    }
}

tasks.register("clean").configure {
    delete(rootProject.buildDir)
}