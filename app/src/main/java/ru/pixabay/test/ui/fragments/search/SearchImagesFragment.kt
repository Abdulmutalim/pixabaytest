package ru.pixabay.test.ui.fragments.search

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.doOnPreDraw
import androidx.core.view.updatePadding
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.paging.LoadState
import androidx.paging.PagingSource
import androidx.paging.PagingState
import androidx.recyclerview.widget.*
import androidx.transition.Fade
import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import ru.pixabay.data.datasource.network.ApiResponse
import ru.pixabay.data.datasource.network.request
import ru.pixabay.models.Images
import ru.pixabay.models.enums.LayoutManagerType
import ru.pixabay.models.eventbusmodels.ApplyImagesFiltersEvent
import ru.pixabay.test.R
import ru.pixabay.test.databinding.FragmentSearchImagesBinding
import ru.pixabay.test.ui.adapters.ImagesAdapter
import ru.pixabay.test.ui.adapters.InitialLoadAdapter
import ru.pixabay.test.ui.adapters.LoadAdapter
import ru.pixabay.test.ui.fragments.BaseFragment
import ru.pixabay.test.ui.fragments.IMAGE_POS_STATE
import ru.pixabay.test.ui.fragments.ImageDetailsFragmentArgs
import ru.pixabay.test.ui.fragments.RECT_SAVE_STATE
import ru.pixabay.test.ui.util.*
import ru.pixabay.test.viewmodels.SearchImagesViewModel

@AndroidEntryPoint
class SearchImagesFragment : BaseFragment<FragmentSearchImagesBinding>() {

    private val viewModel: SearchImagesViewModel by viewModels()

    private val adapter = ImagesAdapter(::toImageDetails)
    private var searchJob: Job? = null
    private var keyboardShowedFirstTime = false

    private var scrollDistForKeyboard = 0
    private var keyboardVisibility = true
    private val keyboardVisibilityActionScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            scrollDistForKeyboard += dy

            if (scrollDistForKeyboard == 0) {
                keyboardVisibility = true
                requireContext().showKeyboard(binding.searchET)
            } else if (keyboardVisibility) {
                keyboardVisibility = false
                insetController.hide(WindowInsetsCompat.Type.ime())
            }
        }
    }

    private val imagesDelegate = ImagesListSharedElementDelegate { binding.searchRV }.apply {
        setFragment(this@SearchImagesFragment)
        this.adapter = this@SearchImagesFragment.adapter
    }
    private var viewTransitionRect
        get() = imagesDelegate.viewTransitionRect
        set(value) {
            imagesDelegate.viewTransitionRect = value
        }
    private var adapterViewTransitionPos
        get() = imagesDelegate.adapterViewTransitionPos
        set(value) {
            imagesDelegate.adapterViewTransitionPos = value
        }
    private var firstVisibleItemPos = -1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_search_images, container, false)
        binding.vm = viewModel
        binding.fragment = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        exitTransition = null
        savedInstanceState?.run {
            if (!isEmpty){
                viewTransitionRect = getParcelable(RECT_SAVE_STATE) ?: Rect()
                adapterViewTransitionPos = getInt(IMAGE_POS_STATE, -1)
            }
        }
        binding.run {

            adapter.applyLayoutManager(
                viewModel.getLayoutManagerType(),
                searchRV
            )

            val footer = LoadAdapter(adapter)
            val initialLoadAdapter = InitialLoadAdapter(3, R.layout.images_load_initial_items)
            adapter.addLoadStateListener { loadStates ->
                footer.loadState = loadStates.append
                initialLoadAdapter.loadState = loadStates.refresh

                if (loadStates.refresh is LoadState.NotLoading && adapter.itemCount == 0) {
                    viewModel.empty.set(true)
                } else {
                    viewModel.empty.set(false)
                }
            }
            searchRV.adapter = ConcatAdapter(initialLoadAdapter, adapter, footer)

            searchRV.addOnScrollListener(keyboardVisibilityActionScrollListener)

            searchET.setOnEditorActionListener { _, id, _ ->
                if (id == EditorInfo.IME_ACTION_DONE) {
                    searchJob?.cancel()
                    adapter.refresh()
                }
                false
            }


            searchET.requestFocus()
            if (!keyboardShowedFirstTime) {
                requireContext().showKeyboard(searchET)
                keyboardShowedFirstTime = true
            }
        }

        viewModel.searchStr.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                searchJob?.cancel()
                if (viewModel.searchStr.get().isNullOrEmpty() ||
                    viewModel.searchStr.get() == viewModel.lastSearchQuery
                ) return
                searchJob = lifecycleScope.launch {
                    delay(1000)
                    adapter.refresh()
                }
            }
        })


        postponeEnterTransition()
        lifecycleScope.launch {
            viewModel.searchImagesPagingSource.collect {
                adapter.submitData(lifecycle, it)
            }
        }
        (view.parent as? ViewGroup)?.doOnPreDraw {
            startPostponedEnterTransition()
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(RECT_SAVE_STATE, viewTransitionRect)
        outState.putInt(IMAGE_POS_STATE, adapterViewTransitionPos)
        super.onSaveInstanceState(outState)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onDestroyView() {
        binding.searchRV.removeOnScrollListener(keyboardVisibilityActionScrollListener)
        super.onDestroyView()
    }

    override fun applyInsets(inset: WindowInsetsCompat) {
        binding.run {
            val ins = inset.getInsets(WindowInsetsCompat.Type.systemBars())
            val searchETCntHeight = ins.top + requireContext().dip(50)
            appBar.requestNewSize(height = searchETCntHeight)
            searchETCnt.applyMargin(top = inset.getInsets(WindowInsetsCompat.Type.statusBars()).top)
            searchRV.updatePadding(bottom = ins.bottom)
            root.updatePadding(left = ins.left, right = ins.right)
        }
    }

    @Subscribe(sticky = true, priority = 0)
    fun refreshOnFiltersEvent(event: ApplyImagesFiltersEvent) {
        EventBus.getDefault().removeStickyEvent(event)
        adapter.refresh()
    }

    fun toImageSearchFilters(){
        navController.navigate(R.id.searchImagesFiltersFragment, Bundle.EMPTY, defNavOption)
    }

    fun toVideoSearch(){
        navController.navigate(R.id.searchVideosFragment, Bundle.EMPTY, defNavOption)
    }

    private fun toImageDetails(image: Images.Image, view: View, needPostpone: Boolean, pos: Int) {
        view.getLocalVisibleRect(viewTransitionRect)
        view.clipBounds = viewTransitionRect
        adapterViewTransitionPos = pos

        val extras = FragmentNavigatorExtras(view to image.largeImageURL)
        exitTransition = Fade()
        navController.navigate(
            R.id.imageDetailsFragment,
            ImageDetailsFragmentArgs(image, needPostpone, adapter.managerType is LayoutManagerType.Linear).toBundle(),
            null,
            extras
        )
    }

    private fun findFirstVisibleItemPos() {
        when(val manager = binding.searchRV.layoutManager) {
            is LinearLayoutManager -> {
                firstVisibleItemPos = manager.findFirstVisibleItemPosition()
            }
            is GridLayoutManager -> {
                firstVisibleItemPos = manager.findFirstVisibleItemPosition()
            }
            is StaggeredGridLayoutManager -> {
                firstVisibleItemPos = manager.findFirstVisibleItemPositions(null)[0]
            }
            is ChipsLayoutManager -> {
                firstVisibleItemPos = manager.findFirstVisibleItemPosition()
            }
        }
    }
}

class ImagesSearchPagingSource(private val vm: SearchImagesViewModel) :
    PagingSource<Int, Images.Image>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Images.Image> {
        val page = params.key ?: 1

        if (vm.searchStr.get().isNullOrEmpty()) {
            return LoadResult.Page(emptyList(), null, null)
        }

        return when (val response = request { vm.loadImages(page) }) {
            is ApiResponse.Result<*> -> {
                val data = response.data as Images
                LoadResult.Page(
                    data.hits,
                    if (page > 1) page - 1 else null,
                    if (page * 20 > data.totalHits) null else page + 1
                )
            }
            is ApiResponse.Error -> {
                LoadResult.Error(Throwable(response.desc))
            }
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Images.Image>): Int {
        return 1
    }
}