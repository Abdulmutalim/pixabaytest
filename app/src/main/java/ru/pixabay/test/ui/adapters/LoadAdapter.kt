package ru.pixabay.test.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.pixabay.test.R
import ru.pixabay.test.databinding.ProgressCircularItemBinding
import ru.pixabay.test.ui.util.visibleIf

class LoadAdapter(private val adapter: PagingDataAdapter<*, *>) : LoadStateAdapter<LoadAdapter.ProgressHolderCircular>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState,
    ): ProgressHolderCircular {
        return ProgressHolderCircular(
            ProgressCircularItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ProgressHolderCircular, loadState: LoadState) {
        when (loadState) {
            is LoadState.Error -> holder.bind(visibilityFlag = false, error = true, mErrorText = loadState.error.message ?: "Ошибка")
            else -> holder.bind(!loadState.endOfPaginationReached, false, mErrorText = "")
        }
    }

    inner class ProgressHolderCircular(private val view: ProgressCircularItemBinding) : RecyclerView.ViewHolder(view.root) {

        init {
            view.run {
                retryBtn.setOnClickListener {
                    adapter.retry()
                }
            }
        }

        fun bind(visibilityFlag: Boolean, error: Boolean, mErrorText: String) {
            view.run {
                errorCnt.visibleIf(error)
                progress.visibleIf(visibilityFlag && !error)
                errorText.text = mErrorText
            }
        }
    }
}