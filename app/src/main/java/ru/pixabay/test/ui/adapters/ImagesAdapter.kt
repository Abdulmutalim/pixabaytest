package ru.pixabay.test.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.ViewCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.*
import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager
import com.google.android.material.color.MaterialColors
import ru.pixabay.models.Images
import ru.pixabay.models.enums.ClipBounds
import ru.pixabay.models.enums.LayoutManagerType
import ru.pixabay.test.App
import ru.pixabay.test.R
import ru.pixabay.test.databinding.ImageItemBinding
import ru.pixabay.test.ui.util.*

class ImagesAdapter(private val toImageDetails: (Images.Image, View, needPotspone: Boolean, pos: Int) -> Unit) :
    PagingDataAdapter<Images.Image, ImagesAdapter.ImageHolder>(ImageDiffCallback) {

    var managerType: LayoutManagerType = LayoutManagerType.Linear

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageHolder {
        val binding = ImageItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return when(managerType) {
            is LayoutManagerType.Linear -> LinearImageHolder(binding)
            is LayoutManagerType.Grid -> GridImageHolder(binding)
            is LayoutManagerType.Staggered -> StaggeredImageHolder(binding)
            is LayoutManagerType.RicardoFlex -> RicardoFlexImageHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: ImageHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isNotEmpty() && payloads[0] is ClipBounds.Clear) {
            holder.clearClipBounds()
        } else {
            super.onBindViewHolder(holder, position, payloads)
        }
    }

    override fun onBindViewHolder(holder: ImageHolder, position: Int) {
        holder.bind(getItem(position)!!)
    }

    abstract inner class ImageHolder(protected val binding: ImageItemBinding) :
        RecyclerView.ViewHolder(binding.image) {

        private lateinit var image: Images.Image
        private var imageLoadAnyStatus = false
        private val glideLoadStatus = object : GlideLoadStatus {
            override fun imageLoadStatus(success: Boolean) {
                imageLoadAnyStatus = success
            }
        }

        protected val context: Context
            get() = binding.root.context

        init {
            binding.image.setOnClickListener {
                toImageDetails(
                    image,
                    binding.image,
                    imageLoadAnyStatus,
                    absoluteAdapterPosition
                )
            }
            binding.image.clipToOutline = true
        }

        fun bind(mImage: Images.Image) {
            imageLoadAnyStatus = false
            image = mImage
            binding.run {
                ViewCompat.setTransitionName(binding.image, mImage.largeImageURL)
                handleImageSize(mImage)
                image.loadImageOriginal(mImage.largeImageURL, glideLoadStatus)
            }
        }

        fun clearClipBounds() {
            binding.image.clipBounds = null
        }

        abstract fun handleImageSize(mImage: Images.Image)
    }

    inner class LinearImageHolder(binding: ImageItemBinding) : ImageHolder(binding) {

        init {
            binding.image.setBackgroundResource(R.drawable.image_round_corners)
        }

        override fun handleImageSize(mImage: Images.Image) {
            val ratio = mImage.imageHeight.toFloat() / mImage.imageWidth.toFloat()
            val height = ((App.screenWidth) * ratio).toInt()
            binding.image.requestNewSize(height = height)
        }
    }

    inner class GridImageHolder(binding: ImageItemBinding) : ImageHolder(binding) {

        init {
            binding.image.setBackgroundColor(
                MaterialColors.getColor(
                    context,
                    R.attr.imageViewBackgroundColor,
                    ""
                )
            )
        }

        override fun handleImageSize(mImage: Images.Image) {
            val height = App.screenWidth / 3 - context.dip(2) * 2
            binding.image.requestNewSize(height = height)
        }
    }

    inner class StaggeredImageHolder(binding: ImageItemBinding) : ImageHolder(binding) {

        init {
            binding.image.setBackgroundColor(
                MaterialColors.getColor(
                    context,
                    R.attr.imageViewBackgroundColor,
                    ""
                )
            )
        }

        override fun handleImageSize(mImage: Images.Image) {
            binding.image.requestNewSize(height = mImage.imageHeight / 5)
        }
    }

    inner class RicardoFlexImageHolder(binding: ImageItemBinding) : ImageHolder(binding) {

        init {
            binding.image.setBackgroundColor(
                MaterialColors.getColor(
                    context,
                    R.attr.imageViewBackgroundColor,
                    ""
                )
            )
        }

        override fun handleImageSize(mImage: Images.Image) {
            val ratioWidth = mImage.imageWidth.toFloat() / mImage.imageHeight.toFloat()
            val factor = when {
                bindingAdapterPosition % 2 == 0 -> 30
                bindingAdapterPosition % 3 == 0 -> 0
                else -> -30
            }
            val width = (context.dip(100 + factor) * ratioWidth).toInt() - context.dip(4)
            binding.image.requestNewSize(width = width, height = context.dip(170))
        }
    }

    object ImageDiffCallback : DiffUtil.ItemCallback<Images.Image>() {
        override fun areItemsTheSame(oldItem: Images.Image, newItem: Images.Image): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Images.Image, newItem: Images.Image): Boolean {
            return oldItem == newItem
        }
    }

    fun applyLayoutManager(type: LayoutManagerType, rv: RecyclerView) {
        managerType = type
        try {
            val d = rv.getItemDecorationAt(0)
            rv.removeItemDecoration(d)
        } catch (e: IndexOutOfBoundsException) {
        }
        when (type) {
            is LayoutManagerType.Linear -> {
                rv.layoutManager = LinearLayoutManager(rv.context)
                rv.addItemDecoration(
                    DividerItemDecoration(rv.context, DividerItemDecoration.VERTICAL).apply {
                        setDrawable(
                            AppCompatResources.getDrawable(
                                rv.context,
                                R.drawable.linear_manager_divider
                            )!!
                        )
                    }
                )
            }
            is LayoutManagerType.RicardoFlex -> {
                rv.layoutManager = ChipsLayoutManager.newBuilder(rv.context)
                    .setScrollingEnabled(true)
                    .setMaxViewsInRow(4)
                    .setOrientation(ChipsLayoutManager.HORIZONTAL)
                    .setRowStrategy(ChipsLayoutManager.STRATEGY_FILL_VIEW)
                    .withLastRow(true)
                    .build()
                rv.addItemDecoration(
                    SpaceAroundItemDecorator(rv.context.dip(1))
                )
            }
            is LayoutManagerType.Grid -> {
                rv.layoutManager = GridLayoutManager(rv.context, 3)
                rv.addItemDecoration(
                    GridDividerItemDecoration(rv.context.dip(2), 3)
                )
            }
            LayoutManagerType.Staggered -> {
                rv.layoutManager =
                    StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
                rv.addItemDecoration(
                    SpaceAroundItemDecorator(rv.context.dip(1))
                )
            }
        }
    }
}

