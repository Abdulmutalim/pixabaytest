package ru.pixabay.test.ui

import android.content.res.Resources
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsControllerCompat
import dagger.hilt.android.AndroidEntryPoint
import ru.pixabay.test.App
import ru.pixabay.test.R

@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main), ActivityProviding {
    private val insets by lazy {
        WindowInsetsControllerCompat(window, window.decorView)
    }

    override fun provideInsetsController() = insets

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.screenWidth = Resources.getSystem().displayMetrics.widthPixels
        fullscreen()
    }

    private fun fullscreen() {
        window.apply {
            WindowCompat.setDecorFitsSystemWindows(this, false)
            statusBarColor = Color.TRANSPARENT
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
                navigationBarColor = Color.TRANSPARENT
            }
        }
    }
}