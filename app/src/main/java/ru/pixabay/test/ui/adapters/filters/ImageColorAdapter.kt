package ru.pixabay.test.ui.adapters.filters

import android.app.Activity
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.StateListDrawable
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.google.android.material.color.MaterialColors
import ru.pixabay.test.R
import ru.pixabay.test.databinding.ColorFilterItemBinding
import ru.pixabay.test.ui.fragments.filters.ImagesFilters
import ru.pixabay.test.ui.util.dip

class ImageColorAdapter(
    private val mContext: Activity,
    val array: Array<ImagesFilters.Color>
) : ArrayAdapter<ImagesFilters.Color>(mContext, R.layout.color_filter_item, array) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val item = array[position]
        val binding: ColorFilterItemBinding =
            if (convertView == null) {
                ColorFilterItemBinding.inflate(mContext.layoutInflater, parent, false)
            } else {
                ColorFilterItemBinding.bind(convertView)
            }
        binding.run {
            colorVisual.background = generateSelectorColorFilter(item.colorRes)
            colorName.text = context.getString(item.colorNameRes)
            colorVisual.isSelected = item.isSelected
            root.setOnClickListener {
                item.isSelected = !item.isSelected
                colorVisual.isSelected = item.isSelected
            }
        }
        return binding.root
    }


    private fun generateSelectorColorFilter(color: Int): StateListDrawable {
        fun generateColorFilterStateDrawable(
            color: Int,
            needStroke: Boolean
        ): GradientDrawable {
            val b = GradientDrawable()
            b.shape = GradientDrawable.OVAL
            b.cornerRadius = mContext.dip(20).toFloat()
            b.setColor(mContext.getColor(color))
            if (needStroke) {
                b.setStroke(
                    mContext.dip(3),
                    MaterialColors.getColor(mContext, R.attr.strokeColorForColorFilter, "")
                )
            }
            return b
        }

        val stateList = StateListDrawable()
        stateList.addState(
            intArrayOf(android.R.attr.state_selected),
            generateColorFilterStateDrawable(color, true)
        )
        stateList.addState(intArrayOf(), generateColorFilterStateDrawable(color, false))
        return stateList
    }
}