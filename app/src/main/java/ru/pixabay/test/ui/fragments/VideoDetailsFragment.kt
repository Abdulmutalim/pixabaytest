package ru.pixabay.test.ui.fragments

import android.Manifest
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import androidx.work.workDataOf
import com.eazypermissions.common.model.PermissionResult
import com.eazypermissions.coroutinespermission.PermissionManager
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.pixabay.models.Videos
import ru.pixabay.test.databinding.FragmentVideoDetailsBinding
import ru.pixabay.test.service.DownloadWorker

class VideoDetailsFragment : BaseFragment<FragmentVideoDetailsBinding>() {

    private val args by lazy { VideoDetailsFragmentArgs.fromBundle(requireArguments()) }
    private val exoPlayer by lazy { ExoPlayer.Builder(requireContext()).build() }
    private val workManager by lazy { WorkManager.getInstance(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentVideoDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.run {
            playerView.controllerHideOnTouch = true
            playerView.useController = true
            playerView.player = exoPlayer

            dropPlayer.setOnClickListener {
                exoPlayer.stop()
                playerView.player = null
                exoPlayer.clearVideoSurface()

                lifecycleScope.launch {
                    playerView.player = exoPlayer
                    exoPlayer.prepare()
                    exoPlayer.play()
                }
            }
        }
        lifecycleScope.launch {
            val a = args.video as Videos.Video
            if (!exoPlayer.isPlaying) {
                val mediaItem = MediaItem.fromUri(a.videos.tiny.url)
                exoPlayer.setMediaItem(mediaItem)
                exoPlayer.prepare()
                exoPlayer.play()
            }
        }
    }

    fun downloadVideo() {
        fun startWorker() {
            val v = args.video as Videos.Video
            val workRequest = OneTimeWorkRequest.Builder(DownloadWorker::class.java)
                .setInputData(
                    workDataOf(
                        DownloadWorker.FILE_TYPE_PARAM to DownloadWorker.DownloadFileType.VIDEO.name,
                        DownloadWorker.FILE_URL_PARAM to v.videos.large,
                        DownloadWorker.FILE_ID_PARAM to v.id
                    )
                )
                .build()
            workManager.enqueue(workRequest)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            startWorker()
        } else {
            lifecycleScope.launch {
                val permissionReq =
                    PermissionManager.requestPermissions(
                        this@VideoDetailsFragment,
                        123,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )

                when (permissionReq) {
                    is PermissionResult.PermissionGranted -> {
                        startWorker()
                    }
                }
            }
        }
    }
}