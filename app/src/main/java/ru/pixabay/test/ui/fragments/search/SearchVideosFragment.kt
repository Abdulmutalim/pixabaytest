package ru.pixabay.test.ui.fragments.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingSource
import androidx.paging.PagingState
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.exoplayer2.ExoPlayer
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import ru.pixabay.data.datasource.network.ApiResponse
import ru.pixabay.data.datasource.network.request
import ru.pixabay.models.Videos
import ru.pixabay.test.R
import ru.pixabay.test.databinding.FragmentSearchVideosBinding
import ru.pixabay.test.ui.adapters.InitialLoadAdapter
import ru.pixabay.test.ui.adapters.LoadAdapter
import ru.pixabay.test.ui.adapters.VideosAdapter
import ru.pixabay.test.ui.fragments.BaseFragment
import ru.pixabay.test.ui.fragments.VideoDetailsFragmentArgs
import ru.pixabay.test.viewmodels.SearchVideosViewModel

@AndroidEntryPoint
class SearchVideosFragment : BaseFragment<FragmentSearchVideosBinding>() {

    private val viewModel: SearchVideosViewModel by viewModels()
    private val exoPlayer by lazy {
        val p = ExoPlayer.Builder(requireContext()).build()
        p.playWhenReady = true
        p
    }
    private val adapter = VideosAdapter(
        {
            navController.navigate(
                R.id.videoDetailsFragment,
                VideoDetailsFragmentArgs(it).toBundle(),
                defNavOption
            )
        },
        { exoPlayer })

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentSearchVideosBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
//            btn.setOnClickListener {
////                downloadVideo()
//                navController.navigate(
//                    R.id.videoDetailsFragment,
//                    VideoDetailsFragmentArgs(video).toBundle(),
//                    defNavOption
//                )
//            }

            val footer = LoadAdapter(adapter)
            val initialLoadAdapter = InitialLoadAdapter(3, R.layout.images_load_initial_items)
            adapter.addLoadStateListener { loadStates ->
                footer.loadState = loadStates.append
                initialLoadAdapter.loadState = loadStates.refresh

                if (loadStates.refresh is LoadState.NotLoading && adapter.itemCount == 0) {
                    viewModel.empty.set(true)
                } else {
                    viewModel.empty.set(false)
                }
            }
            searchRV.adapter = ConcatAdapter(initialLoadAdapter, adapter, footer)
            searchRV.addOnScrollListener(object : RecyclerView.OnScrollListener() {

                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)

                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        playVideo(!searchRV.canScrollVertically(1))
                    }
                }
            })
        }

        lifecycleScope.launch {
            viewModel.searchVideosPagingSource.collect {
                adapter.submitData(lifecycle, it)
            }
        }
    }

    override fun onDestroyView() {
        adapter.currentPlayedViewHolder?.stopVideo()
        super.onDestroyView()
    }

    override fun onDestroy() {
        exoPlayer.release()
        super.onDestroy()
    }

    private fun playVideo(isEndOfList: Boolean) {
        val currentPos =
            (binding.searchRV.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
        val viewHolder = binding.searchRV.findViewHolderForAdapterPosition(currentPos)
                as? VideosAdapter.VideoHolder ?: return
        if (adapter.currentPlayedViewHolder == viewHolder) return
        adapter.currentPlayedViewHolder?.stopVideo()
        viewHolder.playVideo()
        adapter.currentPlayedViewHolder = viewHolder
    }
}

class VideosSearchPagingSource(private val vm: SearchVideosViewModel) :
    PagingSource<Int, Videos.Video>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Videos.Video> {
        val page = params.key ?: 1

//        if (vm.searchStr.get().isNullOrEmpty()) {
//            return LoadResult.Page(emptyList(), null, null)
//        }

        return when (val response = request { vm.searchVideos(page) }) {
            is ApiResponse.Result<*> -> {
                val data = response.data as Videos
                LoadResult.Page(
                    data.hits,
                    if (page > 1) page - 1 else null,
                    if (page * 20 > data.totalHits) null else page + 1
                )
            }
            is ApiResponse.Error -> {
                LoadResult.Error(Throwable(response.desc))
            }
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Videos.Video>): Int {
        return 1
    }
}