package ru.pixabay.test.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.ScrollView

class LockableScrollView @JvmOverloads constructor(
    context: Context,
    attr: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ScrollView(context, attr, defStyleAttr) {

    var lockScroll = false

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        if (lockScroll) requestDisallowInterceptTouchEvent(true)
        else requestDisallowInterceptTouchEvent(false)
        return super.onInterceptTouchEvent(ev)
    }
}