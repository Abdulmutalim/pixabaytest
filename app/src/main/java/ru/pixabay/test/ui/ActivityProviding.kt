package ru.pixabay.test.ui

import androidx.core.view.WindowInsetsControllerCompat
import androidx.navigation.NavController

interface ActivityProviding {
    fun provideInsetsController(): WindowInsetsControllerCompat
}