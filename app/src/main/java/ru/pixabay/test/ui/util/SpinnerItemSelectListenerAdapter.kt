package ru.pixabay.test.ui.util

import android.view.View
import android.widget.AdapterView

open class SpinnerItemSelectListenerAdapter : AdapterView.OnItemSelectedListener {
    open fun itemSelected(pos: Int) {}

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        itemSelected(p2)
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }
}