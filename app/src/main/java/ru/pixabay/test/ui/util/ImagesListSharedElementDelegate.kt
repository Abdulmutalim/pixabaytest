package ru.pixabay.test.ui.util

import android.animation.ValueAnimator
import android.graphics.Rect
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.animation.addListener
import androidx.core.app.SharedElementCallback
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.pixabay.models.enums.ClipBounds
import ru.pixabay.test.R

class ImagesListSharedElementDelegate(private inline val recycler: () -> RecyclerView) {

    var adapter: RecyclerView.Adapter<*>? = null
    var viewTransitionRect = Rect()
    var adapterViewTransitionPos = -1


    fun setFragment(fragment: Fragment) {
        fragment.setExitSharedElementCallback(object : SharedElementCallback() {
            override fun onMapSharedElements(
                names: MutableList<String>?,
                sharedElements: MutableMap<String, View>?
            ) {
                super.onMapSharedElements(names, sharedElements)
                val imageView = sharedElements?.get(names?.getOrNull(0)) as? ImageView ?: return
                animateCardViewOnExitTransition(imageView)
                updateClipBoundsOnExitTransition(imageView)
            }
        })
    }

    private fun animateCardViewOnExitTransition(image: ImageView) {
        adapter?.run {
            fun clearClipBounds() {
                if (adapterViewTransitionPos != -1) {
                    notifyItemChanged(
                        adapterViewTransitionPos,
                        ClipBounds.Clear()
                    )
                }
            }
            if (recycler().layoutManager is GridLayoutManager || recycler().layoutManager !is LinearLayoutManager) {
                image.postDelayed({ clearClipBounds() }, 310)
                return
            }
            val drawableOriginal = image.background
            val drawableTransition =
                AppCompatResources.getDrawable(
                    image.context,
                    R.drawable.image_transition_rounded_coreners
                ) as GradientDrawable
            image.background = drawableTransition

            val animator = ValueAnimator.ofFloat(0f, image.context.dip(20).toFloat())
            animator.duration = 300
            animator.addUpdateListener {
                drawableTransition.cornerRadius = it.animatedValue as Float
            }
            animator.addListener(onEnd = {
                image.background = drawableOriginal
                clearClipBounds()
            })
            animator.start()
        }
    }

    private fun updateClipBoundsOnExitTransition(image: ImageView) {
        val vh = recycler().findViewHolderForAdapterPosition(adapterViewTransitionPos)
        if (vh != null) {
            val rect = Rect()
            vh.itemView.getLocalVisibleRect(rect)
            image.clipBounds = rect
        } else if (!viewTransitionRect.isEmpty) {
            image.clipBounds = viewTransitionRect
        }
    }
}