package ru.pixabay.test.ui.fragments.filters

import ru.pixabay.test.R

object Filters {
    enum class Order(val res: Int, val order: String){
        POPULAR(R.string.order_popular, "popular"),
        LATEST(R.string.order_latest, "latest")
    }

    enum class Category(val res: Int, val category: String){
        NONE(R.string.category_none, ""),
        BACKGROUNDS(R.string.category_backgrounds, "backgrounds"),
        FASHION(R.string.category_fashion, "fashion"),
        NATURE(R.string.category_nature, "nature"),
        SCIENCE(R.string.category_science, "science"),
        EDUCATION(R.string.category_education, "education"),
        FEELINGS(R.string.category_feelings, "feelings"),
        HEALTH(R.string.category_health, "health"),
        PEOPLE(R.string.category_people, "people"),
        RELIGION(R.string.category_religion, "religion"),
        PLACES(R.string.category_places, "places"),
        ANIMALS(R.string.category_animals, "animals"),
        INDUSTRY(R.string.category_industry, "industry"),
        COMPUTER(R.string.category_computer, "computer"),
        FOOD(R.string.category_food, "food"),
        SPORTS(R.string.category_sports, "sports"),
        TRANSPORTATION(R.string.category_transportation, "transportation"),
        TRAVEL(R.string.category_travel, "travel"),
        BUILDINGS(R.string.category_buildings, "buildings"),
        BUSINESS(R.string.category_business, "business"),
        MUSIC(R.string.category_music, "music")
    }
}

object ImagesFilters {
    enum class Type(val res: Int, val type: String) {
        ALL(R.string.image_type_all, "all"),
        PHOTO(R.string.image_type_photo, "photo"),
        ILLUSTRATION(R.string.image_type_illustration, "illustration"),
        VECTOR(R.string.image_type_vector, "vector")
    }

    enum class Orientation(val res: Int, val orientation: String) {
        ALL(R.string.image_orientation_all,"all"),
        HORIZONTAL(R.string.image_orientation_horizontal,"horizontal"),
        VERTICAL(R.string.image_orientation_vertical,"vertical")
    }

    enum class Color(val colorRes: Int, val colorNameRes: Int, val value: String, var isSelected: Boolean = false) {
        RED(R.color.red, R.string.red, "red"),
        ORANGE(R.color.orange, R.string.orange, "orange"),
        YELLOW(R.color.yellow, R.string.yellow, "yellow"),
        GREEN(R.color.green, R.string.green, "green"),
        TURQUOISE(R.color.turquoise, R.string.turquoise, "turquoise"),
        BLUE(R.color.blue, R.string.blue, "blue"),
        LILAC(R.color.lilac, R.string.lilac, "lilac"),
        PINK(R.color.pink, R.string.pink, "pink"),
        GRAY(R.color.gray, R.string.gray, "gray"),
        BROWN(R.color.brown, R.string.brown, "brown")
    }
}