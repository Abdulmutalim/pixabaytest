package ru.pixabay.test.ui.adapters.filters

import android.content.Context
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import ru.pixabay.test.R
import ru.pixabay.test.ui.fragments.filters.ImagesFilters

class ImageOrientationAdapter(
    private val mContext: Context,
    val array: Array<ImagesFilters.Orientation>
) :
    ArrayAdapter<ImagesFilters.Orientation>(mContext, R.layout.filters_item, array) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = super.getView(position, convertView, parent) as TextView
        view.text = mContext.getString(array[position].res)
        view.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = super.getView(position, convertView, parent) as TextView
        view.text = mContext.getString(array[position].res)
        return view
    }
}