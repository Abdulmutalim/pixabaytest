package ru.pixabay.test.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import ru.pixabay.models.Videos
import ru.pixabay.test.App
import ru.pixabay.test.R
import ru.pixabay.test.databinding.VideoItemBinding
import ru.pixabay.test.ui.util.requestNewSize

class VideosAdapter(private val toVideo: (Videos.Video) -> Unit, private inline val exoPlayerCallback: () -> ExoPlayer) :
    PagingDataAdapter<Videos.Video, VideosAdapter.VideoHolder>(VideoDiffCallback) {

    var currentPlayedViewHolder: VideoHolder? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoHolder {
        return VideoHolder(
            VideoItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: VideoHolder, position: Int) {
        holder.bind(getItem(position)!!)
    }

    override fun onViewRecycled(holder: VideoHolder) {
        super.onViewRecycled(holder)
        if (currentPlayedViewHolder == holder) {
            holder.stopVideo()
        }
    }

    inner class VideoHolder(private val binding: VideoItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        lateinit var video: Videos.Video

        init {
            binding.run {
                playerView.useController = false
            }
        }

        fun bind(mVideo: Videos.Video) {
            this.video = mVideo
            binding.run {
                val ratio = video.videos.tiny.height.toFloat() / video.videos.tiny.width.toFloat()
                val height = ((App.screenWidth) * ratio).toInt()
                playerView.requestNewSize(height = height)
                root.setOnClickListener {
                    toVideo(video)
                }
            }
        }

        fun playVideo() {
            val mediaItem = MediaItem.fromUri(video.videos.tiny.url)
            exoPlayerCallback().setMediaItem(mediaItem)
            binding.playerView.player = exoPlayerCallback()
            exoPlayerCallback().prepare()
            binding.playingIndicator.setBackgroundColor(binding.playingIndicator.context.getColor(R.color.green))
        }

        fun stopVideo() {
            exoPlayerCallback().stop()
            exoPlayerCallback().clearVideoSurface()
            video.lastPlayerSeek = exoPlayerCallback().contentPosition
            binding.playerView.player = null
            binding.playingIndicator.setBackgroundColor(binding.playingIndicator.context.getColor(R.color.red))
        }
    }

    object VideoDiffCallback : DiffUtil.ItemCallback<Videos.Video>() {
        override fun areItemsTheSame(oldItem: Videos.Video, newItem: Videos.Video): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Videos.Video, newItem: Videos.Video): Boolean {
            return oldItem == newItem
        }
    }
}