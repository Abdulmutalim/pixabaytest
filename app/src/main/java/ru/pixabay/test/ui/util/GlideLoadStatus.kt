package ru.pixabay.test.ui.util

interface GlideLoadStatus {
    fun imageLoadStatus(success: Boolean)
}