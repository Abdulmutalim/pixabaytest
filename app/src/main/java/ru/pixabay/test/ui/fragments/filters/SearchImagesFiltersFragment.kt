package ru.pixabay.test.ui.fragments.filters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.pixabay.test.App
import ru.pixabay.test.databinding.FragmentSearchImagesFiltersBinding
import ru.pixabay.test.ui.adapters.filters.*
import ru.pixabay.test.ui.fragments.BaseFragment
import ru.pixabay.test.ui.util.SpinnerItemSelectListenerAdapter
import ru.pixabay.test.ui.util.dip
import ru.pixabay.test.ui.util.requestNewSize
import ru.pixabay.test.viewmodels.ImageFiltersViewModel
import kotlin.math.ceil

@AndroidEntryPoint
class SearchImagesFiltersFragment : BaseFragment<FragmentSearchImagesFiltersBinding>() {

    private val viewModel: ImageFiltersViewModel by activityViewModels()

    private val orderAdapter by lazy {
        ImageOrderAdapter(
            requireContext(),
            Filters.Order.values()
        )
    }
    private val typeAdapter by lazy {
        ImageTypeAdapter(
            requireContext(),
            ImagesFilters.Type.values()
        )
    }
    private val orientationAdapter by lazy {
        ImageOrientationAdapter(
            requireContext(),
            ImagesFilters.Orientation.values()
        )
    }
    private val categoryAdapter by lazy {
        ImageCategoryAdapter(
            requireContext(),
            Filters.Category.values()
        )
    }
    private val colorsAdapter by lazy {
        ImageColorAdapter(
            requireActivity(),
            viewModel.colorsArray
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchImagesFiltersBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initOrderFilter()
        initTypeFilter()
        initOrientationFilter()
        initCategoryFilter()
        initColorFilters()
        binding.run {
            safeSearchSwich.isChecked = viewModel.safeSearchFilter
            safeSearchSwich.setOnCheckedChangeListener { _, b ->
                viewModel.safeSearchFilter = b
            }

            editorChoiceSwitch.isChecked = viewModel.editorChoiceFilter
            editorChoiceSwitch.setOnCheckedChangeListener { _, b ->
                viewModel.editorChoiceFilter = b
            }

            applyFilters.setOnClickListener {
                viewModel.postFilters()
                navController.popBackStack()
            }
        }
    }

    override fun applyInsets(inset: WindowInsetsCompat) {
        binding.run {
            val ins = inset.getInsets(WindowInsetsCompat.Type.systemBars())
            root.updatePadding(left = ins.left, top = ins.top, right = ins.right, bottom = ins.bottom)
        }
    }

    private fun initOrderFilter() {
        binding.run {
            orderSpinner.adapter = orderAdapter
            orderSpinner.onItemSelectedListener =
                object : SpinnerItemSelectListenerAdapter() {
                    override fun itemSelected(pos: Int) {
                        viewModel.orderFilter = orderAdapter.array[pos].order
                        viewModel.lastOrderSpinnerPos = pos
                    }
                }
            orderSpinner.setSelection(viewModel.lastOrderSpinnerPos)
        }
    }

    private fun initTypeFilter() {
        binding.run {
            typeSpinner.adapter = typeAdapter
            typeSpinner.onItemSelectedListener = object : SpinnerItemSelectListenerAdapter() {
                override fun itemSelected(pos: Int) {
                    viewModel.typeFilter = typeAdapter.array[pos].type
                    viewModel.lastTypeSpinnerPos = pos
                }
            }
            typeSpinner.setSelection(viewModel.lastTypeSpinnerPos)
        }
    }

    private fun initOrientationFilter() {
        binding.run {
            orientationSpinner.adapter = orientationAdapter
            orientationSpinner.onItemSelectedListener =
                object : SpinnerItemSelectListenerAdapter() {
                    override fun itemSelected(pos: Int) {
                        viewModel.orientationFilter = orientationAdapter.array[pos].orientation
                        viewModel.lastOrientationSpinnerPos = pos
                    }
                }
            orientationSpinner.setSelection(viewModel.lastOrientationSpinnerPos)
        }
    }

    private fun initCategoryFilter() {
        binding.run {
            categorySpinner.adapter = categoryAdapter
            categorySpinner.onItemSelectedListener =
                object : SpinnerItemSelectListenerAdapter() {
                    override fun itemSelected(pos: Int) {
                        viewModel.categoryFilter = categoryAdapter.array[pos].category
                        viewModel.lastCategorySpinnerPos = pos
                    }
                }
            categorySpinner.setSelection(viewModel.lastCategorySpinnerPos)
        }
    }

    private fun initColorFilters() {
        binding.run {
            blackAndWhite.isChecked = viewModel.blackAndWhiteFilter
            blackAndWhite.setOnCheckedChangeListener { _, b ->
                viewModel.blackAndWhiteFilter = b
            }

            grayscale.isChecked = viewModel.grayscaleFilter
            grayscale.setOnCheckedChangeListener { _, b ->
                viewModel.grayscaleFilter = b
            }

            transparent.isChecked = viewModel.transparentFilter
            transparent.setOnCheckedChangeListener { _, b ->
                viewModel.transparentFilter = b
            }

            val numColumns = (App.screenWidth - dip(16)) / dip(70)
            colorsGrid.numColumns = numColumns
            colorsGrid.adapter = colorsAdapter

            val rowsCount = ceil(colorsAdapter.array.size.toFloat() / numColumns.toFloat()).toInt()
            val height = rowsCount * dip(40) + rowsCount * dip(8)
            colorsGrid.requestNewSize(height = height)
        }
    }
}