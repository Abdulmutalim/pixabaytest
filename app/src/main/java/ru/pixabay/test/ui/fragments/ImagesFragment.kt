package ru.pixabay.test.ui.fragments

import android.content.Intent
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.doOnPreDraw
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.paging.LoadState
import androidx.paging.PagingSource
import androidx.paging.PagingState
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.transition.Fade
import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import ru.pixabay.data.datasource.network.ApiResponse
import ru.pixabay.data.datasource.network.request
import ru.pixabay.models.Constants
import ru.pixabay.models.Images
import ru.pixabay.models.enums.LayoutManagerType
import ru.pixabay.test.R
import ru.pixabay.test.databinding.ImagesFragmentBinding
import ru.pixabay.test.ui.adapters.ImagesAdapter
import ru.pixabay.test.ui.adapters.InitialLoadAdapter
import ru.pixabay.test.ui.adapters.LoadAdapter
import ru.pixabay.test.ui.util.ImagesListSharedElementDelegate
import ru.pixabay.test.ui.util.applyMargin
import ru.pixabay.test.ui.util.dip
import ru.pixabay.test.ui.util.requestNewSize
import ru.pixabay.test.viewmodels.ImagesViewModel


const val RECT_SAVE_STATE = "imageRect"
const val IMAGE_POS_STATE = "imagePos"

@AndroidEntryPoint
class ImagesFragment : BaseFragment<ImagesFragmentBinding>() {
    private val viewModel: ImagesViewModel by viewModels()

    private val adapter = ImagesAdapter(::toImageDetails)

    private val imagesDelegate = ImagesListSharedElementDelegate { binding.imagesRV }.apply {
        setFragment(this@ImagesFragment)
        this.adapter = this@ImagesFragment.adapter
    }
    private var viewTransitionRect
        get() = imagesDelegate.viewTransitionRect
        set(value) {
            imagesDelegate.viewTransitionRect = value
        }
    private var adapterViewTransitionPos
        get() = imagesDelegate.adapterViewTransitionPos
        set(value) {
            imagesDelegate.adapterViewTransitionPos = value
        }
    private var firstVisibleItemPos = -1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ImagesFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        exitTransition = null
        savedInstanceState?.run {
            if (!isEmpty) {
                viewTransitionRect = getParcelable(RECT_SAVE_STATE) ?: Rect()
                adapterViewTransitionPos = getInt(IMAGE_POS_STATE, -1)
            }
        }
        binding.run {

            adapter.applyLayoutManager(
                viewModel.getLayoutManagerType(),
                imagesRV
            )

            val header = LoadAdapter(adapter)
            val footer = LoadAdapter(adapter)
            val initialLoadAdapter = InitialLoadAdapter(3, R.layout.images_load_initial_items)
            adapter.addLoadStateListener { loadStates ->
                header.loadState = loadStates.prepend
                footer.loadState = loadStates.append
                initialLoadAdapter.loadState = loadStates.refresh

                if (loadStates.refresh is LoadState.Error && adapter.itemCount == 0) {
                    errorState()
                } else {
                    mainState()
                }
            }
            imagesRV.adapter = ConcatAdapter(initialLoadAdapter, header, adapter, footer)


            toSearch.setOnClickListener {
                navController.navigate(
                    R.id.searchImagesFragment,
                    null,
                    defNavOption
                )
            }
            refresh.setOnClickListener {
                adapter.refresh()
            }
            pixabaySource.setOnClickListener {
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(Constants.PIXABAY_SITE_URL)
                startActivity(i)
            }
            toSettings.setOnClickListener {
                firstVisibleItemPos = findFirstVisibleItemPos()
                imagesRV.isSaveEnabled = false
                navController.navigate(R.id.imagesLayoutManagerSettingFragment, Bundle.EMPTY, defNavOption)
            }
        }

        postponeEnterTransition()
        lifecycleScope.launch {
            viewModel.imagesPagingSource.collect {
                adapter.submitData(lifecycle, it)
            }
        }
        (view.parent as? ViewGroup)?.doOnPreDraw {
            startPostponedEnterTransition()
            if (firstVisibleItemPos != -1) {
                binding.imagesRV.scrollToPosition(firstVisibleItemPos)
                firstVisibleItemPos = -1
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(RECT_SAVE_STATE, viewTransitionRect)
        outState.putInt(IMAGE_POS_STATE, adapterViewTransitionPos)
        super.onSaveInstanceState(outState)
    }

    override fun onDestroyView() {
        viewModel.safeLastPaginationPage(findFirstVisibleItemPos()/20+1)
        super.onDestroyView()
    }

    override fun applyInsets(inset: WindowInsetsCompat) {
        binding.run {
            val ins = inset.getInsets(WindowInsetsCompat.Type.systemBars())

            val searchETCntHeight = ins.top + requireContext().dip(50)
            appBar.requestNewSize(height = searchETCntHeight)
            topbarCnt.applyMargin(top = ins.top)
            imagesRV.updatePadding(bottom = ins.bottom)
            root.updatePadding(left = ins.left, right = ins.right)
        }
    }

    private fun toImageDetails(image: Images.Image, view: View, needPostpone: Boolean, pos: Int) {
        view.getLocalVisibleRect(viewTransitionRect)
        view.clipBounds = viewTransitionRect
        adapterViewTransitionPos = pos

        val extras = FragmentNavigatorExtras(view to image.largeImageURL)
        exitTransition = Fade()
        navController.navigate(
            R.id.imageDetailsFragment,
            ImageDetailsFragmentArgs(
                image,
                needPostpone,
                adapter.managerType is LayoutManagerType.Linear
            ).toBundle(),
            null,
            extras
        )
    }

    private fun errorState() {
        binding.run {
            error.isVisible = true
            imagesRV.isVisible = false
        }
    }

    private fun mainState() {
        binding.run {
            error.isVisible = false
            imagesRV.isVisible = true
        }
    }

    private fun findFirstVisibleItemPos(): Int {
        return when(val manager = binding.imagesRV.layoutManager) {
            is LinearLayoutManager -> {
                manager.findFirstCompletelyVisibleItemPosition()
            }
            is GridLayoutManager -> {
                manager.findFirstCompletelyVisibleItemPosition()
            }
            is StaggeredGridLayoutManager -> {
                manager.findFirstCompletelyVisibleItemPositions(null)[0]
            }
            is ChipsLayoutManager -> {
                manager.findFirstCompletelyVisibleItemPosition()
            }
            else -> -1
        }
    }
}

class ImagesPagingSource(private val vm: ImagesViewModel) : PagingSource<Int, Images.Image>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Images.Image> {
        val page = params.key ?: 1
        return when (val response = request { vm.loadImages(page) }) {
            is ApiResponse.Result<*> -> {
                val data = response.data as Images
                LoadResult.Page(
                    data.hits,
                    if (page > 1) page - 1 else null,
                    //Так и не нашел в апи кол-во доступных страниц, пришлось самому вычислять
                    if (page * 20 > data.totalHits) null else page + 1
                )
            }
            is ApiResponse.Error -> {
                //Не знал какую логику для картинок в БД написать, решил подгружжать из бд, если тупо какаято ошибка
                val localImages = vm.loadDBImages()
                if (localImages.isNotEmpty()) {
                    LoadResult.Page(
                        localImages,
                        if (page > 1) page - 1 else null,
                        page + vm.imagesDBPages
                    )
                } else {
                    LoadResult.Error(Throwable(response.desc))
                }
            }
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Images.Image>): Int {
//        Log.e("refresh", "refresh")
//        if (vm.refreshAllData) {
//            vm.refreshAllData = false
//            return 1
//        }
//        return state.anchorPosition?.let { anchorPosition ->
//            val anchorPage = state.closestPageToPosition(anchorPosition)
//            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
//        } ?: 1
        return 1
    }
}