package ru.pixabay.test.ui.util

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class SpaceAroundItemDecorator(private val spaceAroundPx: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.set(spaceAroundPx, spaceAroundPx, spaceAroundPx, spaceAroundPx)
    }
}