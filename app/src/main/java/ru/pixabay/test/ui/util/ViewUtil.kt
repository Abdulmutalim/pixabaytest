package ru.pixabay.test.ui.util

import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.view.TouchDelegate
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.marginLeft
import androidx.core.view.marginRight
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.google.android.material.color.MaterialColors
import ru.pixabay.test.R


fun View.requestNewSize(
    width: Int? = null, height: Int? = null
) {
    if (width != null) {
        layoutParams.width = width
    }
    if (height != null) {
        layoutParams.height = height
    }
    layoutParams = layoutParams
}

fun View.applyMargin(
    start: Int? = null,
    top: Int? = null,
    end: Int? = null,
    bottom: Int? = null
) {
    if (layoutParams is ViewGroup.MarginLayoutParams) {
        layoutParams = (layoutParams as ViewGroup.MarginLayoutParams).apply {
            setMargins(start ?: marginLeft, top ?: topMargin, end ?: marginRight, bottom ?: bottomMargin)
        }
    }
}


@BindingAdapter("loadImage", "loadListener", requireAll = false)
fun ImageView.loadImage(imageUri: String, loadListener: GlideLoadStatus? = null) {
    val circularProgressDrawable = CircularProgressDrawable(context)
    circularProgressDrawable.strokeWidth = 5f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.setColorSchemeColors(MaterialColors.getColor(this, R.attr.progressBarColor))
    circularProgressDrawable.start()

    Glide.with(context)
        .applyDefaultRequestOptions(
            RequestOptions()
                .dontTransform()
                .dontAnimate()
                .placeholder(circularProgressDrawable)
                .error(R.drawable.ic_image_load_error)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
        )
        .load(imageUri)
        .listener(object : RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                loadListener?.imageLoadStatus(false)
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                loadListener?.imageLoadStatus(true)
                return false
            }
        })
        .into(this)
}

@BindingAdapter("loadImageOriginalSize", "loadListener", requireAll = false)
fun ImageView.loadImageOriginal(imageUri: String, loadListener: GlideLoadStatus? = null) {
    val circularProgressDrawable = CircularProgressDrawable(context)
    circularProgressDrawable.strokeWidth = 5f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.setColorSchemeColors(MaterialColors.getColor(this, R.attr.progressBarColor))
    circularProgressDrawable.start()

    Glide.with(context)
        .applyDefaultRequestOptions(
            RequestOptions()
                .dontTransform()
                .dontAnimate()
                .placeholder(circularProgressDrawable)
                .error(R.drawable.ic_image_load_error)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .format(DecodeFormat.PREFER_ARGB_8888)
                .override(Target.SIZE_ORIGINAL)
        )
        .load(imageUri)
        .listener(object : RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                loadListener?.imageLoadStatus(false)
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                loadListener?.imageLoadStatus(true)
                return false
            }
        })
        .into(this)
}

@BindingAdapter("loadCircleImage", "loadListener", requireAll = false)
fun ImageView.loadCircleImage(url: String?, loadListener: GlideLoadStatus? = null) {
    val circularProgressDrawable = CircularProgressDrawable(context)
    circularProgressDrawable.strokeWidth = 5f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.setColorSchemeColors(MaterialColors.getColor(this, R.attr.progressBarColor))
    circularProgressDrawable.start()

    Glide.with(context)
        .load(url)
        .apply(RequestOptions
            .centerCropTransform()
            .placeholder(circularProgressDrawable as Drawable)
            .error(R.drawable.ic_image_load_error)
            .circleCrop()
        )
        .listener(object : RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                loadListener?.imageLoadStatus(false)
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                loadListener?.imageLoadStatus(true)
                return false
            }
        })
        .into(this)

}

@BindingAdapter("invisibleIf")
fun View.invisibleIf(boolean: Boolean) {
    visibility = if (boolean) View.INVISIBLE else View.VISIBLE
}

@BindingAdapter("visibleIf")
fun View.visibleIf(boolean: Boolean) {
    visibility = if (boolean) View.VISIBLE else View.GONE
}

@BindingAdapter("goneIf")
fun View.goneIf(boolean: Boolean) {
    visibility = if (boolean) View.GONE else View.VISIBLE
}

fun View.applyTouchDelegate(left: Int = 0, top: Int = 0, right: Int = 0, bottom: Int = 0) {
    val mParent = parent as View
    mParent.post {
        val rect = Rect()
        getHitRect(rect)
        rect.left -= left
        rect.top -= top
        rect.right += right
        rect.bottom += bottom
        mParent.touchDelegate = TouchDelegate(rect, this)
    }
}