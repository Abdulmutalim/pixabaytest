package ru.pixabay.test.ui.fragments.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.pixabay.models.enums.LayoutManagerType
import ru.pixabay.test.R
import ru.pixabay.test.databinding.FragmentImageLayoutManagerSettingBinding
import ru.pixabay.test.ui.fragments.BaseFragment
import ru.pixabay.test.ui.util.applyMargin
import ru.pixabay.test.viewmodels.SettingsViewModel

@AndroidEntryPoint
class ImagesLayoutManagerSettingFragment : BaseFragment<FragmentImageLayoutManagerSettingBinding>() {

    private val viewModel: SettingsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentImageLayoutManagerSettingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            group.check(getCheckIdByType())
            group.setOnCheckedChangeListener { group, checkedId ->
                when(checkedId) {
                    R.id.linear -> viewModel.saveLayoutManagerType(LayoutManagerType.Linear)
                    R.id.grid -> viewModel.saveLayoutManagerType(LayoutManagerType.Grid)
                    R.id.staggered -> viewModel.saveLayoutManagerType(LayoutManagerType.Staggered)
                    R.id.flex -> viewModel.saveLayoutManagerType(LayoutManagerType.RicardoFlex)
                }
            }
        }
    }

    private fun getCheckIdByType(): Int {
        return when(viewModel.getLayoutManagerType()){
            is LayoutManagerType.Linear -> R.id.linear
            LayoutManagerType.Grid -> R.id.grid
            LayoutManagerType.Staggered -> R.id.staggered
            LayoutManagerType.RicardoFlex -> R.id.flex
        }
    }

    override fun applyInsets(inset: WindowInsetsCompat) {
        val ins = inset.getInsets(WindowInsetsCompat.Type.systemBars())
        binding.root.updatePadding(left = ins.left, top = ins.top, right = ins.right)
    }
}