package ru.pixabay.test.ui.fragments

import android.Manifest
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.Bundle
import android.view.*
import android.view.animation.DecelerateInterpolator
import androidx.core.animation.addListener
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.transition.*
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import androidx.work.workDataOf
import com.eazypermissions.common.model.PermissionResult
import com.eazypermissions.coroutinespermission.PermissionManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import ru.pixabay.models.Images
import ru.pixabay.test.App
import ru.pixabay.test.R
import ru.pixabay.test.databinding.FragmentImageDetailsBinding
import ru.pixabay.test.service.DownloadWorker
import ru.pixabay.test.ui.util.*
import ru.pixabay.test.viewmodels.ImageDetailsViewModel
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

@AndroidEntryPoint
class ImageDetailsFragment : BaseFragment<FragmentImageDetailsBinding>() {

    private val viewModel: ImageDetailsViewModel by viewModels()
    private val args by lazy { ImageDetailsFragmentArgs.fromBundle(requireArguments()) }

    private val loadUserImageStatus = object : GlideLoadStatus {
        override fun imageLoadStatus(success: Boolean) {
            viewModel.userImageViewVisibility.set(success)
        }
    }
    private val loadImageStatus = object : GlideLoadStatus {
        override fun imageLoadStatus(success: Boolean) {
            startPostponedEnterTransition()
        }
    }
    private val workManager by lazy { WorkManager.getInstance(requireContext()) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val transition = TransitionSet()
            .addTransition(ChangeBounds())
            .addTransition(ChangeTransform())
            .addTransition(ChangeClipBounds())
            .addTransition(ChangeImageTransform())
            .setInterpolator(DecelerateInterpolator(1.9f))
            .setDuration(300)
        sharedElementEnterTransition = transition
        sharedElementReturnTransition = transition
        enterTransition = Fade()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_image_details, container, false)
        binding.mImage = args.image
        binding.userImageStatus = loadUserImageStatus
        binding.imageLoadStatus = loadImageStatus
        binding.fragment = this
        binding.vm = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (args.needPostpone) {
            postponeEnterTransition()
        }
        binding.run {
            val mImage: Images.Image = args.image
            ViewCompat.setTransitionName(image, mImage.largeImageURL)
            val ratio = mImage.imageHeight.toFloat() / mImage.imageWidth.toFloat()
            val height = ((App.screenWidth) * ratio).toInt()
            imageCnt.requestNewSize(height = height)
            image.requestNewSize(height = height)
            image.clipToOutline = true

            val imageBackground = image.background as GradientDrawable
            if (args.needCornerImageAnimation) {
                val animator = ValueAnimator.ofFloat(requireContext().dip(20).toFloat(), 0f)
                animator.duration = 300
                animator.addUpdateListener {
                    imageBackground.cornerRadius = it.animatedValue as Float
                }
                animator.addListener(onEnd = {
                    imageCnt.clipToOutline = false
                })
                animator.start()
            } else {
                imageCnt.clipToOutline = false
                imageBackground.cornerRadius = 0f
            }
            initTouchListener()
            downloadImage.applyTouchDelegate(top = dip(20), bottom = dip(20))
        }
    }

    override fun applyInsets(inset: WindowInsetsCompat) {
        binding.run {
            val ins = inset.getInsets(WindowInsetsCompat.Type.systemBars())
            root.updatePadding(left = ins.left, top = ins.top, right = ins.right)
        }
    }

    fun downloadImage() {
        fun startWorker() {
            val workRequest = OneTimeWorkRequest.Builder(DownloadWorker::class.java)
                .setInputData(
                    workDataOf(
                        DownloadWorker.FILE_TYPE_PARAM to DownloadWorker.DownloadFileType.IMAGE.name,
                        DownloadWorker.FILE_URL_PARAM to args.image.largeImageURL,
                        DownloadWorker.FILE_ID_PARAM to args.image.id
                    )
                )
                .build()
            workManager.enqueue(workRequest)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            startWorker()
        } else {
            lifecycleScope.launch {
                val permissionReq =
                    PermissionManager.requestPermissions(
                        this@ImageDetailsFragment,
                        123,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )

                when (permissionReq) {
                    is PermissionResult.PermissionGranted -> {
                        startWorker()
                    }
                }
            }
        }
    }

    //зачем я это сделал....
    private var lockScroll: Boolean
        get() = binding.scroll.lockScroll
        set(value) {
            binding.scroll.lockScroll = value
        }

    private var scaleFactor = 1f
    private var lastRotation = 0f
    private var scalingInProgress = false
    private var isSingleTouch = true
        set(value) {
            lockScroll = !value
            field = value
        }
    private var closeOnSwipe = false
    private val scaleGestureDetector by lazy {
        ScaleGestureDetector(
            requireContext(),
            object : ScaleGestureDetector.OnScaleGestureListener {
                override fun onScale(p: ScaleGestureDetector?): Boolean {
                    scaleFactor *= p!!.scaleFactor
                    scaleFactor = max(0.1f, min(scaleFactor, 10.0f))
                    binding.run {
                        image.scaleX = scaleFactor
                        image.scaleY = scaleFactor
                    }
                    return true
                }

                override fun onScaleBegin(p: ScaleGestureDetector?): Boolean {
                    scalingInProgress = true
                    closeOnSwipe = false
                    binding.run {
                        scaleFactor = image.scaleX
                        image.pivotY = p!!.focusY
                        image.pivotX = p.focusX
                    }
                    return true
                }

                override fun onScaleEnd(p0: ScaleGestureDetector?) {
                    scalingInProgress = false
                }
            })
    }
    private val scrollGestureDetector by lazy {
        GestureDetector(requireContext(), object : GestureDetector.SimpleOnGestureListener() {
            override fun onScroll(
                e1: MotionEvent?,
                e2: MotionEvent?,
                distanceX: Float,
                distanceY: Float
            ): Boolean {
                binding.run {
                    if (isSingleTouch && image.scaleX == 1f) {
                        image.translationX -= distanceX / 5
                        image.translationY -= distanceY / 5
                        closeOnSwipe = abs(image.translationY).toInt() > requireContext().dip(25)
                    } else {
                        image.translationX -= distanceX
                        image.translationY -= distanceY
                    }
                }
                return super.onScroll(e1, e2, distanceX, distanceY)
            }
        })
    }
    private val rotationGestureDetector by lazy {
        RotationGestureDetector {
            val dRotation = lastRotation - it.angle
            lastRotation = it.angle
            binding.image.rotation += dRotation
        }
    }

    var animator: ViewPropertyAnimator? = null

    @SuppressLint("ClickableViewAccessibility")
    private fun initTouchListener() {
        binding.run {
            imageCnt.setOnTouchListener { view, event ->
                isSingleTouch = event.pointerCount < 2
                rotationGestureDetector.onTouchEvent(event)
                scaleGestureDetector.onTouchEvent(event)
                scrollGestureDetector.onTouchEvent(event)
                if (event.actionMasked == MotionEvent.ACTION_DOWN) {
                    animator?.cancel()
                }
                if (event.actionMasked == MotionEvent.ACTION_UP || event.actionMasked == MotionEvent.ACTION_CANCEL) {
                    lastRotation = 0f
                    if (closeOnSwipe) {
                        navController.popBackStack()
                    } else {
                        animator = image.animate()
                            .scaleX(1f)
                            .scaleY(1f)
                            .translationX(0f)
                            .translationY(0f)
                            .rotation(0f)
                            .setDuration(300)
                        animator?.start()
                    }
                }
                !scalingInProgress
            }
        }
    }
}