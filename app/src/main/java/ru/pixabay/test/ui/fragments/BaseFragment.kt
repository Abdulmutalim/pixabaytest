package ru.pixabay.test.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsAnimationCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.Fragment
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import ru.pixabay.test.R
import ru.pixabay.test.ui.ActivityProviding

abstract class BaseFragment<B : ViewBinding> : Fragment() {
    protected val navController by lazy { findNavController() }
    protected val insetController by lazy { (activity as ActivityProviding).provideInsetsController() }
    private var _binding: B? = null
    protected var binding: B
        get() = requireNotNull(_binding)
        set(value) {
            _binding = value
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewCompat.setOnApplyWindowInsetsListener(view) { _, insets ->
            applyInsets(insets)
            insets
        }
        ViewCompat.setWindowInsetsAnimationCallback(
            view,
            object : WindowInsetsAnimationCompat.Callback(DISPATCH_MODE_STOP) {
                override fun onProgress(
                    insets: WindowInsetsCompat,
                    runningAnimations: MutableList<WindowInsetsAnimationCompat>
                ): WindowInsetsCompat {
                    return insets
                }
            }
        )
    }

    open fun applyInsets(inset: WindowInsetsCompat) {}

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    companion object {
        val defNavOption = NavOptions.Builder()
            .setEnterAnim(R.anim.slide_in)
            .setExitAnim(R.anim.fade_out)
            .setPopEnterAnim(R.anim.fade_in)
            .setPopExitAnim(R.anim.slide_out)
            .build()
    }
}