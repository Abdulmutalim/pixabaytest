package ru.pixabay.test.viewmodels

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableInt
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import ru.pixabay.models.Images
import ru.pixabay.models.eventbusmodels.FileDownloadEnded
import ru.pixabay.models.eventbusmodels.FileDownloadProgressEvent
import ru.pixabay.models.eventbusmodels.FileDownloadStarted
import ru.pixabay.test.service.DownloadWorker
import javax.inject.Inject

@HiltViewModel
class ImageDetailsViewModel @Inject constructor(
    savedState: SavedStateHandle
): ViewModel() {

    init {
        EventBus.getDefault().register(this)
    }

    private val imageId = savedState.get<Images.Image>("image")?.id
    val userImageViewVisibility = ObservableBoolean(false)
    val imageDownloadInProgress = ObservableBoolean( DownloadWorker.downloadingFilesIdSet?.contains(imageId) == true)
    val imageDownloadPercent = ObservableInt(0)

    @Subscribe
    fun fileDownloadStarted(event: FileDownloadStarted){
        if (event.fileId == imageId) {
            imageDownloadInProgress.set(true)
        }
    }

    @Subscribe
    fun fileDownloadProgressEvent(event: FileDownloadProgressEvent){
        if (event.fileId == imageId) {
            imageDownloadPercent.set(event.progress)
        }
    }

    @Subscribe
    fun fileDownloadEnded(event: FileDownloadEnded){
        if (event.fileId == imageId) {
            imageDownloadInProgress.set(false)
        }
    }

    override fun onCleared() {
        EventBus.getDefault().unregister(this)
    }
}