package ru.pixabay.test.viewmodels

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import dagger.hilt.android.lifecycle.HiltViewModel
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import retrofit2.Response
import ru.pixabay.domain.usecase.GetImgLayoutManagerTypeUseCase
import ru.pixabay.domain.usecase.SearchImagesUseCase
import ru.pixabay.models.Images
import ru.pixabay.models.enums.LayoutManagerType
import ru.pixabay.models.eventbusmodels.ApplyImagesFiltersEvent
import ru.pixabay.test.ui.fragments.search.ImagesSearchPagingSource
import javax.inject.Inject

@HiltViewModel
class SearchImagesViewModel @Inject constructor(
    private val savedState: SavedStateHandle,
    private val searchImagesUseCase: SearchImagesUseCase,
    private val getImgLayoutManagerType: GetImgLayoutManagerTypeUseCase
) : ViewModel() {
    val searchStr = ObservableField( savedState["lastSearchQuery"] ?: "")
    val empty = ObservableBoolean(false)
    var lastSearchQuery: String? = null
    private var filtersMap = mapOf("safesearch" to true.toString())
    private var colorsArray: List<String>? = null

    init {
        EventBus.getDefault().register(this)
    }

    val searchImagesPagingSource by lazy {
        Pager(
            PagingConfig(pageSize = 20, initialLoadSize = 20),
            initialKey = 1,
            pagingSourceFactory = {
                ImagesSearchPagingSource(this)
            }
        )
            .flow
            .cachedIn(viewModelScope)
    }

    suspend fun loadImages(page: Int): Response<Images> {
        lastSearchQuery = searchStr.get()
        savedState["lastSearchQuery"] = lastSearchQuery
        return searchImagesUseCase.searchImages(searchStr.get() ?: "", page, colorsArray, filtersMap)
    }

    @Subscribe(priority = 1)
    fun applyFilters(event: ApplyImagesFiltersEvent) {
        filtersMap = event.map
        colorsArray = event.colors
    }

    fun getLayoutManagerType(): LayoutManagerType {
        return getImgLayoutManagerType.get()
    }

    override fun onCleared() {
        EventBus.getDefault().unregister(this)
    }
}