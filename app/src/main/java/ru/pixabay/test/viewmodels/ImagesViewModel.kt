package ru.pixabay.test.viewmodels

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Response
import ru.pixabay.domain.usecase.*
import ru.pixabay.models.Images
import ru.pixabay.models.enums.LayoutManagerType
import ru.pixabay.test.ui.fragments.ImagesPagingSource
import javax.inject.Inject

@HiltViewModel
class ImagesViewModel @Inject constructor(
    private val savedState: SavedStateHandle,
    private val getImagesUseCase: GetImagesUseCase,
    private val insertDBImagesUseCase: InsertDBImagesUseCase,
    private val getDBImagesUseCase: GetDBImagesUseCase,
    private val clearDBImagesUseCase: ClearDBImagesUseCase,
    private val saveImgLayoutManagerTypeUseCase: SaveImgLayoutManagerTypeUseCase,
    private val getImgLayoutManagerTypeUseCase: GetImgLayoutManagerTypeUseCase
) : ViewModel() {

    val imagesPagingSource by lazy {
        Pager(
            PagingConfig(pageSize = 20, initialLoadSize = 20),
            initialKey = savedState["lastPage"],
            pagingSourceFactory = {
                ImagesPagingSource(this)
            }
        )
            .flow
            .cachedIn(viewModelScope)
    }

    suspend fun loadImages(page: Int): Response<Images> {
        val response = getImagesUseCase.loadImages(page)
        viewModelScope.launch {
            if (response.isSuccessful && response.body() != null) {
                imagesDBPages = 0
                if (page == 1) clearDBImagesUseCase.clearImages()
                insertDBImagesUseCase.insertImages(response.body()!!.hits)
            }
        }
        return response
    }

    var imagesDBPages = 0
    suspend fun loadDBImages(): List<Images.Image> {
        val images = getDBImagesUseCase.loadImages(imagesDBPages)
        imagesDBPages++
        return images
    }

    fun saveLayoutManagerType(type: LayoutManagerType) {
        saveImgLayoutManagerTypeUseCase.save(type)
    }

    fun getLayoutManagerType(): LayoutManagerType {
        return getImgLayoutManagerTypeUseCase.get()
    }

    fun safeLastPaginationPage(p: Int) {
        //Этот тип менеджера не может нормально восстановить свой скролл,
        //поэтому как сумасшедший на автомате скролит вверх когда подгружаются
        //итемы сверху
        if (getLayoutManagerType() !is LayoutManagerType.RicardoFlex)
            savedState["lastPage"] = p
    }
}