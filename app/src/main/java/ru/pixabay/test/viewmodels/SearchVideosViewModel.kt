package ru.pixabay.test.viewmodels

import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import dagger.hilt.android.lifecycle.HiltViewModel
import retrofit2.Response
import ru.pixabay.data.datasource.network.ApiResponse
import ru.pixabay.data.datasource.network.request
import ru.pixabay.domain.usecase.SearchVideosUseCase
import ru.pixabay.models.Videos
import ru.pixabay.test.ui.fragments.search.VideosSearchPagingSource
import javax.inject.Inject

@HiltViewModel
class SearchVideosViewModel @Inject constructor(
    private val searchVideosUseCase: SearchVideosUseCase
) : ViewModel() {

    val empty = ObservableBoolean(false)
    val searchStr = ObservableField("")
    private var filtersMap = mapOf("safesearch" to true.toString())

    val searchVideosPagingSource by lazy {
        Pager(
            PagingConfig(pageSize = 20, initialLoadSize = 20),
            initialKey = 1,
            pagingSourceFactory = {
                VideosSearchPagingSource(this)
            }
        )
            .flow
            .cachedIn(viewModelScope)
    }

    suspend fun loadVideos(
        query: String,
        page: Int,
        queryMap: Map<String, String>
    ): Videos.Video {
        val q = request { searchVideosUseCase.search(query, page, queryMap) }
        when (q) {
            is ApiResponse.Result<*> -> {
                val data = q.data as Videos
                return data.hits[0]
                Log.e("VIDEOS_COUNT", data.hits.size.toString())
            }
            is ApiResponse.Error -> {
                throw IllegalStateException()
            }
        }
    }

    suspend fun searchVideos(
        page: Int
    ): Response<Videos> {
        return searchVideosUseCase.search(searchStr.get() ?: "", page, filtersMap)
    }
}