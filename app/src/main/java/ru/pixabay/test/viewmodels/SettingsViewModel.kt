package ru.pixabay.test.viewmodels

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import ru.pixabay.domain.usecase.GetImgLayoutManagerTypeUseCase
import ru.pixabay.domain.usecase.SaveImgLayoutManagerTypeUseCase
import ru.pixabay.models.enums.LayoutManagerType
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val saveImgLayoutManagerTypeUseCase: SaveImgLayoutManagerTypeUseCase,
    private val getImgLayoutManagerTypeUseCase: GetImgLayoutManagerTypeUseCase
) : ViewModel() {

    fun getLayoutManagerType() : LayoutManagerType {
        return getImgLayoutManagerTypeUseCase.get()
    }

    fun saveLayoutManagerType(type: LayoutManagerType) {
        saveImgLayoutManagerTypeUseCase.save(type)
    }
}