package ru.pixabay.test.viewmodels

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import org.greenrobot.eventbus.EventBus
import ru.pixabay.models.eventbusmodels.ApplyImagesFiltersEvent
import ru.pixabay.test.ui.fragments.filters.Filters
import ru.pixabay.test.ui.fragments.filters.ImagesFilters
import javax.inject.Inject

@HiltViewModel
class ImageFiltersViewModel @Inject constructor() : ViewModel() {

    var orderFilter = Filters.Order.POPULAR.order
    var lastOrderSpinnerPos = 0

    var typeFilter = ImagesFilters.Type.ALL.type
    var lastTypeSpinnerPos = 0

    var orientationFilter = ImagesFilters.Orientation.ALL.orientation
    var lastOrientationSpinnerPos = 0

    var categoryFilter = Filters.Category.NONE.category
    var lastCategorySpinnerPos = 0

    val colorsArray = ImagesFilters.Color.values()
    var blackAndWhiteFilter = false
    var grayscaleFilter = false
    var transparentFilter = false

    var safeSearchFilter = true
    var editorChoiceFilter = false


    fun postFilters() {
        val map = mapOf(
            "order" to orderFilter,
            "image_type" to typeFilter,
            "orientation" to orientationFilter,
            "category" to categoryFilter,
            "safesearch" to safeSearchFilter.toString(),
            "editors_choice" to editorChoiceFilter.toString()
        )


        val colorsArray = ArrayList<String>(colorsArray.filter { it.isSelected }.map { it.value })
        if (blackAndWhiteFilter){
            colorsArray.add("black")
            colorsArray.add("white")
        }

        if (grayscaleFilter) {
            colorsArray.add("grayscale")
        }

        if (transparentFilter) {
            colorsArray.add("transparent")
        }

        EventBus.getDefault().postSticky(ApplyImagesFiltersEvent(map, colorsArray))
    }
}