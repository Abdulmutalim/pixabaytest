package ru.pixabay.test.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.pixabay.data.repository.ImagesRepositoryImpl
import ru.pixabay.data.repository.VideosRepositoryImpl
import ru.pixabay.domain.repository.ImagesRepository
import ru.pixabay.domain.repository.VideosRepository

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
    @Binds
    abstract fun bindImagesRepository(repo: ImagesRepositoryImpl): ImagesRepository

    @Binds
    abstract fun bindVideosRepository(repo: VideosRepositoryImpl): VideosRepository
}