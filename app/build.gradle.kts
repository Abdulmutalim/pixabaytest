plugins {
    id(BuildPlugins.androidApplication)
    id(BuildPlugins.kotlinAndroid)
    id(BuildPlugins.kotlinKapt)
    id(BuildPlugins.daggerHiltAndroid)
    id(BuildPlugins.kotlinSerialization)
    id(BuildPlugins.kotlinParcelize)
    id(BuildPlugins.navComponentSafeArgs)
}

android {
    val needDebugProguard = false
    compileSdk = 31

    defaultConfig {
        applicationId = "ru.pixabay.test"
        minSdk = 23
        targetSdk = 31
        versionCode = 1
        versionName = "1.0"
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            buildConfigField("String", "PIXABAY_API_KEY", "\"23669754-11c86f7c5c0d5f99bc6978371\"")
            buildConfigField("String", "BASE_URL", "\"https://pixabay.com/api/\"")
            buildConfigField("String", "VIDEO_IMAGE_PREVIEW_BASE_URL", "\"https://i.vimeocdn.com/video/\"")
        }

        debug {
            if (needDebugProguard) {
                isMinifyEnabled = true
                isShrinkResources = true
                proguardFiles(
                    getDefaultProguardFile("proguard-android-optimize.txt"),
                    "proguard-rules.pro"
                )
            }
            buildConfigField("String", "PIXABAY_API_KEY", "\"23669754-11c86f7c5c0d5f99bc6978371\"")
            buildConfigField("String", "BASE_URL", "\"https://pixabay.com/api/\"")
            buildConfigField("String", "VIDEO_IMAGE_PREVIEW_BASE_URL", "\"https://i.vimeocdn.com/video/\"")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_11.toString()
    }
    buildFeatures {
        dataBinding = true
        viewBinding = true
    }
}

dependencies {
    implementation(project(":domain"))
    implementation(project(":data"))
    implementation(project(":models"))

    implementation(Dependencies.fragmentKtx)
    implementation(Dependencies.coreKtx)
    implementation(Dependencies.appCompat)
    implementation(Dependencies.material)
    implementation(Dependencies.constraintLayout)
    implementation(Dependencies.legacySupport)
    implementation(Dependencies.livecycleLiveDataKtx)
    implementation(Dependencies.lifecycleViewModelKtx)
    implementation(Dependencies.lifecycleExtensions)

    implementation(Dependencies.pagingLibrary)

    implementation(Dependencies.eventBus)
    implementation(Dependencies.workKtx)

    //room
    implementation(Dependencies.room)
    implementation(Dependencies.roomKtx)
    kapt(Dependencies.roomCompiler)

    //Nav Component
    implementation(Dependencies.navigationFragment)
    implementation(Dependencies.navigationUiKtx)

    //dagger hilt
    implementation(Dependencies.hilt)
    kapt(Dependencies.hiltCompiler)
    implementation(Dependencies.hiltWork)
    kapt(Dependencies.hiltWorkCompiler)

    //network
    implementation(Dependencies.retrofit)

    //glide
    implementation(Dependencies.glide)
    kapt(Dependencies.glideCompiler)

    implementation(Dependencies.coroutinesPermissions)

    implementation(Dependencies.ricardoFlexLayoutManager)

    implementation(Dependencies.exoPlayer)
}

kapt {
    correctErrorTypes = true
}